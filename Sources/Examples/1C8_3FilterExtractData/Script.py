import sys
sys.path.append('../../')
import GUI
import keyboard
import subprocess
import time
#TimeStart - 7:45 TimeEnd - 8:50 Actions = 17
#Maximize 1C
GUI.UIOSelector_Get_UIO([{"title":"1С:Предприятие - Логистика Маслов Иван","class_name":"V8TopLevelFrame","backend":"uia"}]).maximize()

#Filter data
#################
#FocusHighlight 1C
GUI.UIOSelector_FocusHighlight([{"title":"1С:Предприятие - Логистика Маслов Иван","class_name":"V8TopLevelFrame","backend":"uia"},{"ctrl_index":3},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":1},{"ctrl_index":0},{"ctrl_index":5}])
#Click_input filter icon
GUI.UIOSelector_Get_UIO([{"title":"1С:Предприятие - Логистика Маслов Иван","class_name":"V8TopLevelFrame","backend":"uia"},{"ctrl_index":3},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":1},{"ctrl_index":0},{"ctrl_index":5}]).click_input()

#Selecte filter- month begin
GUI.UIOSelector_Get_UIO([{"title":"Настройка периода","class_name":"V8NewLocalFrameBaseWnd","backend":"uia"},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":3},{"ctrl_index":0},{"ctrl_index":17}]).click_input()

#Click OK
GUI.UIOSelector_Get_UIO([{"title":"Настройка периода","class_name":"V8NewLocalFrameBaseWnd","backend":"uia"},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":5}]).click()

#Loop for 5 items
lLoopIterator = 0
while lLoopIterator<5:
    lItemIndex=lLoopIterator*9+1
    #FocusHighlight
    GUI.UIOSelector_FocusHighlight([{"title":"1С:Предприятие - Логистика Маслов Иван","class_name":"V8TopLevelFrame","backend":"uia"},{"ctrl_index":3},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":lItemIndex}])
    #Double Click - open form
    GUI.UIOSelector_Get_UIO([{"title":"1С:Предприятие - Логистика Маслов Иван","class_name":"V8TopLevelFrame","backend":"uia"},{"ctrl_index":3},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":lItemIndex}]).double_click_input()
    #Get grid structured texts
    lItemListTexts=GUI.UIOSelector_Get_UIO([{"title":"1С:Предприятие - Логистика Маслов Иван","class_name":"V8TopLevelFrame","backend":"uia"},{"ctrl_index":3},{"backend":"uia"},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":1}]).texts()
    #Close form
    GUI.UIOSelector_Get_UIO([{"title":"1С:Предприятие - Логистика Маслов Иван","class_name":"V8TopLevelFrame","backend":"uia"},{"ctrl_index":3},{"backend":"uia"},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":9},{"ctrl_index":0},{"ctrl_index":3}]).click_input()
    #Focus NotePad
    GUI.UIOSelector_FocusHighlight([{"title":"Безымянный — Блокнот","class_name":"Notepad","backend":"uia"},{"ctrl_index":0}])
    #Write Task number
    keyboard.write("Задание: "+GUI.UIOSelector_Get_UIO([{"title":"1С:Предприятие - Логистика Маслов Иван","class_name":"V8TopLevelFrame","backend":"uia"},{"ctrl_index":3},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":lItemIndex}]).texts()[0])
    #Type enter
    keyboard.press_and_release('enter')
    #write ItemList
    keyboard.write("Перечень номенклатуры: ")
    #Type enter
    keyboard.press_and_release('enter')
    for lItemListItem in lItemListTexts:
        keyboard.write(lItemListItem)
        keyboard.press_and_release('enter')
    #Type double enter
    keyboard.press_and_release('enter')
    keyboard.press_and_release('enter')
    #FocusHighlight 1C
    GUI.UIOSelector_FocusHighlight([{"title":"1С:Предприятие - Логистика Маслов Иван","class_name":"V8TopLevelFrame","backend":"uia"},{"ctrl_index":3},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":0},{"ctrl_index":1},{"ctrl_index":0},{"ctrl_index":5}])
    #Increment loop iterator
    lLoopIterator=lLoopIterator+1
    
#Highlight the UI Object in Folder explorer
#GUI.UIOSelector_FocusHighlight([{"class_name":"CabinetWClass","backend":"uia"},{"ctrl_index":2},{"ctrl_index":0},{"ctrl_index":2},{"ctrl_index":0}])
#Wait 2 seconds
#time.sleep(3)
#Loop: get child element of UI List
#for lItem in GUI.UIOSelector_Get_UIO([{"class_name":"CabinetWClass","backend":"uia"},{"ctrl_index":2},{"ctrl_index":0},{"ctrl_index":2},{"ctrl_index":0}]).children():
#    print(str(lItem))