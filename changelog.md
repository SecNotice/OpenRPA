[1.1.0]
After 2 month test prefinal with new improovements (+RobotRDPActive in Orchestrator + Easy ControlPanelTemplate)
Beta before 1.1.0 (new way of OpenRPA with improovments. Sorry, but no backward compatibility)/ Backward compatibility will start from 1.0.1
[1.0.37]
Minor fix in RobotRDPActive
[1.0.33]
Manu changes - look git
[1.0.31]
Orchestrator new engine - test 2 is ready. Go to PIP
[1.0.30]
RobotScreenActive - robot, which monitor the active screen and run Console session if screen disappear
[1.0.29]
RobotRDPActive minor Fix in str conv
[1.0.28]
RobotRDPActive first version is ready!
[1.0.26]
Robot UIDesktop bug fix in Safe other process function
[1.0.25]
*Dont upload to PyPi* - Not tested
Created safe call function in UIDesktop
UIOSelector_SafeOtherGet_Process

Safe call in UIDesktop for:
- UIOSelectorUIOActivity_Run_Dict
- UIOSelector_Exist_Bool
- UIOSelector_Highlight
- UIOSelector_FocusHighlight
- UIOSelector_SearchChildByMouse_UIOTree
- UIOSelector_GetChildList_UIOList
- UIOSelector_Get_UIOInfoList
- UIOSelector_Get_UIOInfo
- UIOSelector_Get_UIOActivityList

UIOSelectorSecs_WaitAppear_Bool
UIOSelectorSecs_WaitDisappear_Bool
UIOSelectorsSecs_WaitAppear_List
UIOSelectorsSecs_WaitDisappear_List
[1.0.24]
1.0.1 Beta
Refactoring (Studio Orchestrator in pyOpenRPA package)
[1.0.22]
1.0.1 Beta
[1.0.19]
MinorFix in pyOpenRPA.Core
[1.0.18]
MinorFix in pyOpenRPA.Core