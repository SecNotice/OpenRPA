def Settings():
    lResult={
        "pyOpenRPA":{
            "Robot":{
                "UIDesktop":{
                    "Utils":{
                        "ProcessBitness":{
                            "Python32FullPath": "..\\..\\..\\Resources\\WPy32-3720\\python-3.7.2\\python.exe", #Set from user: "..\\Resources\\WPy32-3720\\python-3.7.2\\OpenRPARobotGUIx32.exe"
                            "Python64FullPath": None, #Set from user
                            "Python32ProcessName": "OpenRPA_UIDesktopX32.exe", #Config set once
                            "Python64ProcessName": "OpenRPA_UIDesktopX64.exe" #Config set once
                        }
                    }
                }
            }
        }
    }
    return lResult