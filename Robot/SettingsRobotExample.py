import logging
#Robot settings
from pyOpenRPA.Robot import OrchestratorConnector
import os
import logging
import datetime
#Definitions
lOrchestratorHost="localhost"
lOrchestratorPort=8081
lOrchestratorProtocol="http"
lOrchestratorAuthToken="1992-04-03-0643-ru-b4ff-openrpa52zzz"
#Robot settings
def Settings():
    import os
    mDict = {
        "ProcessBitness": {
            "Python32FullPath": None, #Set from user: "..\\Resources\\WPy32-3720\\python-3.7.2\\OpenRPARobotGUIx32.exe"
            "Python64FullPath": None, #Set from user
            "Python32ProcessName": "OpenRPAUIDesktopX32.exe", #Config set once
            "Python64ProcessName": "OpenRPAUIDesktopX64.exe" #Config set once
        },
        "Logger": logging.getLogger("Robot"),
        "OrchestratorToRobotStorage": { 
        
        },
        "OrchestratorToRobotResetStorage": { 
            "SafeTurnOff":False #Control from orchestrator to safety turn off robot
        },
        "OrchestratorConnector": {
            #Fill below
        },
        "OrchestratorConnectorTerminateAll":OrchestratorConnector.IntervalTermimateAll #Call this function when program must be shutted down (to kill threads from OrchestratorConnector)
    }
    ######################
    #OrchestratorConnector
    ######################
    mDict["OrchestratorConnector"]={
        "IntervalDataSendResetAsync": [
            {
                "Interval": 2,
                "RobotStorage": mGlobal["Storage"],
                "RobotStorageKey": "R01_OrchestratorToRobot",
                "RobotResetValue": {"Test": "Test"},
                "OrchestratorKeyList": ["Storage", "R01_OrchestratorToRobot"],
                "OrchestratorProtocol": lOrchestratorProtocol,
                "OrchestratorHost": lOrchestratorHost,
                "OrchestratorPort": lOrchestratorPort,
                "OrchestratorAuthToken": lOrchestratorAuthToken
            }
        ]
    }
    #Turn off many warnings from orchestrator connector logger
    OrchestratorConnector.LoggerSetLevelError()
    #Run OrchestratorConnector initialization
    OrchestratorConnector.ConfigurationInit(mDict["OrchestratorConnector"])
    #########################
    #Создать файл логирования
    # add filemode="w" to overwrite
    if not os.path.exists("Reports"):
        os.makedirs("Reports")
    ##########################
    #Подготовка логгера Robot
    #########################
    mRobotLogger=mDict["Logger"]
    mRobotLogger.setLevel(logging.INFO)
    # create the logging file handler
    mRobotLoggerFH = logging.FileHandler("Reports\ReportRobot_"+datetime.datetime.now().strftime("%Y_%m_%d")+".log")
    mRobotLoggerFormatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    mRobotLoggerFH.setFormatter(mRobotLoggerFormatter)
    # add handler to logger object
    mRobotLogger.addHandler(mRobotLoggerFH)
    ############################################
    return mDict