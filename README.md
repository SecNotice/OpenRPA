# OpenRPA
First open source RPA platform for business is released!

# How to run
Studio
Double click to Studio\StudioRun_32.cmd or Studio\StudioRun_64.cmd

# Robot how to debug
Robot\PythonDebug_64.cmd
import RobotConnector
RobotConnector.ActivityRun(
	{
	   ModuleName: <"UIDesktop"|..., str>,
	   ActivityName: <Function or procedure name in module, str>,
	   ArgumentList: [<Argument 1, any type>, ...] - optional,
	   ArgumentDict: {<Argument 1 name, str>:<Argument 1 value, any type>, ...} - optional
	}
)

OR

from pyOpenRPA.Robot import UIDesktop
UIDesktop.<ActivityName>(<args>)

# Robot example script:
Robot\Examples\GetFolderList\Python_32_Script_Run.cmd

# Python 32 bit
Resources\WPy32-3720\python-3.7.2\python.exe

# Python 64 bit
Resources\WPy64-3720\python-3.7.2.amd64\python.exe

# Module UIDesktop activity List:
############################
Новая версия
############################
Получить список элементов, который удовлетворяет условиям через расширенный движок поиска
[
   {
       "index":<Позиция элемента в родительском объекте>,
       "depth_start" - глубина, с которой начинается поиск (по умолчанию 1)
       "depth_end" - глубина, до которой ведется поиск (по умолчанию 1)
       "class_name" - наименование класса, который требуется искать
       "title" - наименование заголовка
       "rich_text" - наименование rich_text
   }
]


# Open RPA Wiki
## Content
In wiki you can find:
- [About OpenRPA, library dependencies and licensing](https://gitlab.com/UnicodeLabs/OpenRPA/wikis/01.-About-OpenRPA,-library-dependencies-and-licensing)
- [Architecture (Studio, Robot, Orchestrator)](https://gitlab.com/UnicodeLabs/OpenRPA/wikis/02.-Architecture-(Studio,-Robot,-Orchestrator))
- [How to install (system requirements)](https://gitlab.com/UnicodeLabs/OpenRPA/wikis/03.-How-to-install-(system-requirements))
- [Tool Studio: How to use](https://gitlab.com/UnicodeLabs/OpenRPA/wikis/04.1.-Tool-Studio:-How-to-use)
- [Tool Robot: How to use](https://gitlab.com/UnicodeLabs/OpenRPA/wikis/04.2.-Tool-Robot:-How-to-use)
- Tool Orchestrator: How to use
- [Theory & practice: Web app access (Chrome, Firefox, Opera)](https://gitlab.com/UnicodeLabs/OpenRPA/wikis/05.1.-Theory-&-practice:-Web-app-access-(Chrome,-Firefox,-Opera))
- [Theory & practice: Desktop app UI access (win32 and UI automation dlls)](https://gitlab.com/UnicodeLabs/OpenRPA/wikis/05.2.-Theory-&-practice:-Desktop-app-UI-access-(win32-and-UI-automation-dlls))
- Theory & practice: Keyboard & mouse manipulation
- Theory & practice: Screen capture & image recognition

#Dependencies
*  Python 3 x32 [psutil, pywinauto, wmi, PIL, keyboard, pyautogui, win32api (pywin32), selenium, openCV, tesseract, requests, lxml, PyMuPDF]
*  Python 3 x64 [psutil, pywinauto, wmi, PIL, keyboard, pyautogui, win32api (pywin32), selenium, openCV, tesseract, requests, lxml, PyMuPDF]
*  pywinauto (Windows GUI automation)
*  Semantic UI CSS framework
*  JsRender by https://www.jsviews.com (switch to Handlebars)
*  Handlebars

Created by Unicode Labs (Ivan Maslov)