import psutil, datetime, logging, os, json
# # # # # # # ORCHESTRATOR CONTROL PANEL <Robot name> # # # # # # #
# Init parameters
gRobotStartFilePathStr = r"path\to\start\file" # path\to\start\file
gRobotProcessNameWOEXEStr = "process name" # RobotProcessName
gRobotADLoginStr = "Login" # Login of the robot session
gRobotADPasswordStr = "Password" # Password for session
gRobotKeyStr = "Robot_Template"
gRDPSessionKeyStr = "RDP_Template"
gRDPSessionHostStr = "localhost" # Rdp session host
gRDPSessionPortStr = "3389" # Default 3389
gControlPanelKeyStr = "ControlPanel_Template"
gControlPanelCheckRobotProcessFromOrchestratorUserBool = True # Check process activity from orchestrator GUI session (with help of task manager, when users on the same machine)
# !! ATTENTION !! SCHEDULE TIME START STOP FILL BELOW IN ACTIVITY SECTION
gRobotToOrchestratorKeyList = ["Storage",gRobotKeyStr,"RobotToOrchestrator"]
gOrchestratorToRobotResetKeyList = ["Storage",gRobotKeyStr,"OrchestratorToRobotReset"]
gOrchestratorToRobotResetSafeStopKeyList = gOrchestratorToRobotResetKeyList+["SafeTurnOffBool"]

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Activity list
## Robot START (create RDP session, send CMD to start process)
gActivityROBOTStartTimeHH_MMStr = "03:00" # Time "HH:MM" [Server time] to execute activity, example "05:10"
gActivityROBOTStartWeekdayList = [0,1,2,3,4] # WeekdayList when atcivity is starting, default [0,1,2,3,4,5,6]
gActivityROBOTStartList = [
    { # Start RDP Session
        "DefNameStr":"RDPSessionConnect", # Function name in RobotRDPActive.Processor
        "ArgList":[], # Args list
        "ArgDict":{"inRDPSessionKeyStr": gRDPSessionKeyStr, "inHostStr": gRDPSessionHostStr, "inPortStr": gRDPSessionPortStr, "inLoginStr": gRobotADLoginStr, "inPasswordStr": gRobotADPasswordStr} # Args dictionary
    },
    { # Run robot file in RDP session
        "DefNameStr":"RDPSessionProcessStartIfNotRunning", # Function name in RobotRDPActive.Processor
        "ArgList":[], # Args list
        "ArgDict":{"inRDPSessionKeyStr": gRDPSessionKeyStr, "inProcessNameWEXEStr": f"{gRobotProcessNameWOEXEStr}.exe", "inFilePathStr": gRobotStartFilePathStr, "inFlagGetAbsPathBool": False} # Args dictionary
    }
]
## Robot FORCE STOP (send CMD to stop process, logoff)
gActivityROBOTStopTimeHH_MMStr = "08:00" # Time "HH:MM" [Server time] to execute activity, example "05:10"
gActivityROBOTStopWeekdayList = [0,1,2,3,4] # WeekdayList when atcivity is starting, default [0,1,2,3,4,5,6]
gActivityROBOTStopList = [
    { # Kill process
        "DefNameStr":"RDPSessionProcessStop", # Function name in RobotRDPActive.Processor
        "ArgList":[], # Args list
        "ArgDict":{"inRDPSessionKeyStr": gRDPSessionKeyStr, "inProcessNameWEXEStr":f"{gRobotProcessNameWOEXEStr}.exe","inFlagForceCloseBool": True} # Args dictionary
    },
    { # Logoff RDP Session
        "DefNameStr":"RDPSessionLogoff", # Function name in RobotRDPActive.Processor
        "ArgList":[], # Args list
        "ArgDict":{"inRDPSessionKeyStr": gRDPSessionKeyStr} # Args dictionary
    }
]
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Function, which is generate Dict for front-endswith
def ControlPanelRenderDict(inGSettings):
    """result={
        "HeaderLeftText":"<Robot name>",
        "HeaderRightText":"<header>",
        "DataStorageKey":"Robot_Name", #Use key for set current dict in mGlobal.DataStorage["DataStorageKey"] on client side
        "SubheaderText": "State: <span style=\"color:green\">Turned on</span>",
        "BodyKeyValueList":[
            {"Key":"Session list","Value":""},
            {"Key":"Session list","Value":""}
        ],
        "FooterText":"Last update: 9:38:00 09.10.2019",
        "FooterButtonX2List":[
            {"Text":"Turn on", "Color":"green", "Link":"", "OnClick": lOnClickRunButton.replace("\\","\\\\")},
        ],
        "FooterButtonX1List":[
            {"Text":"Kill", "Color":"red", "Link":"", "OnClick": lOnClickForceCloseButton.replace("\\","\\\\")}
        ]
    }"""
    # START :: Create activities :: START #
    ## Robot START (create RDP session, send CMD to start process)
    lActivityROBOTStartEscaped = f"mGlobal.Processor.ServerValueOperatorPlus({json.dumps(['RobotRDPActive','ActivityList'])},{json.dumps(gActivityROBOTStartList)});".replace("\"","\'")
    ## Robot SAFE STOP (SAFE STOP COMMAND (FROM ROBOT RULES), Logoff must do robot when safe turn off)
    lActivityROBOTSafeStopEscaped = f"mGlobal.Processor.ServerValueOperatorPlus({json.dumps(gOrchestratorToRobotResetSafeStopKeyList)},true);".replace("\"","\'")
    ## Robot FORCE STOP (send CMD to stop process, logoff)
    lActivityROBOTStopEscaped = f"mGlobal.Processor.ServerValueOperatorPlus({json.dumps(['RobotRDPActive','ActivityList'])},{json.dumps(gActivityROBOTStopList)});".replace("\"","\'")
    # END :: Create activities :: END #
    # START :: Init result dict template :: START #
    # lBodyKeyValue_r3_start=f'<a onclick="{lActivityROBOTStartEscaped}" style=\"color:green\">Start</a>'
    lResultDict={
        "HeaderLeftText":"ROBOT KEYWORD",
        "HeaderRightText":"ROBOT NAME",
        "DataStorageKey":gControlPanelKeyStr, # CLIENT SIDE:: Use key for set current dict in mGlobal.DataStorage["DataStorageKey"] on client side
        "SubheaderText":"<Subheader text, state filled below>",
        "BodyKeyValueList":[
            {"Key":"Scheduler","Value":""},
            {"Key":"Start","Value":f"at {gActivityROBOTStartTimeHH_MMStr} in weekdays {gActivityROBOTStartWeekdayList}"},
            {"Key":"Stop (kill)","Value":f"at {gActivityROBOTStopTimeHH_MMStr} in weekdays {gActivityROBOTStopWeekdayList}"},
            {"Key":"Statistic","Value":""},
            {"Key":"...","Value":""}
        ],
        "FooterText":"Last update: 9:38:00 09.10.2019",
        "FooterButtonX2List":[
            {"Text":"Turn on", "Color":"green", "Link":"", "OnClick": lActivityROBOTStartEscaped},
            {"Text":"Safe turn off", "Color":"orange", "Link":"", "OnClick": lActivityROBOTSafeStopEscaped}
        ],
        "FooterButtonX1List":[
            {"Text":"Kill", "Color":"red", "Link":"", "OnClick": lActivityROBOTStopEscaped}
        ],
        # "GlobalStorage": inGSettings.get("Storage",{}) # UNCOMMENT FOR DEBUG PURPOSE TO WATCH inGSettings on client side
    }
    # END :: Init result dict template :: END #
    # START :: Fill BodyKeyValueList :: START #

    # END :: Fill BodyKeyValueList :: END #
    # START :: Fill SubheaderText :: START #
    ## FILL Robot state by the check the RDP session state
    lSubheaderRunTrueText="State: <span style=\"color:green\">Turned on</span>"
    lSubheaderRunFalseText="State: <span style=\"color:red\">Turned off</span>"
    if gControlPanelCheckRobotProcessFromOrchestratorUserBool and gRDPSessionKeyStr in inGSettings["RobotRDPActive"]["RDPList"]:
        lResultDict["SubheaderText"]=lSubheaderRunTrueText
    else:
        lResultDict["SubheaderText"]=lSubheaderRunFalseText
    # END :: Fill SubheaderText :: END #
    # Fill FooterText
    lResultDict["FooterText"]=f'Last update: {datetime.datetime.now().strftime("%H:%M:%S %d.%m.%Y")}'
    return lResultDict

# Technical def - Get item by the list of keys
def TechDictKeyList_ItemGet(inDict, inKeyList, inDefault={}):
    lResult=inDict
    for lItem in inKeyList:
        if lResult:
            lResult = lResult.get(lItem,None)
    if lResult is None:
        lResult=inDefault
    return lResult
# # # # # # # # # # # # # # # # # # # # # # # #
# Scheduler to start/stop process
## Start ROBOT at time
lActivityTimeItemRobotStart = {
    "TimeHH:MM": gActivityROBOTStartTimeHH_MMStr, #Time [HH:MM] to trigger activity
    "WeekdayList": gActivityROBOTStartWeekdayList, #List of the weekday index when activity is applicable, Default [1,2,3,4,5,6,7]
    "Activity":{
        "Type": "GlobalDictKeyListValueOperator+", #Activity type
        "KeyList": ['RobotRDPActive','ActivityList'], # RobotRDP Active  ActivityList
        "Value": gActivityROBOTStartList # ActivityList - see upper
    }
}
## Force stop ROBOT at time
lActivityTimeItemRobotStop = {
    "TimeHH:MM": gActivityROBOTStopTimeHH_MMStr, #Time [HH:MM] to trigger activity
    "WeekdayList": gActivityROBOTStopWeekdayList, #List of the weekday index when activity is applicable, Default [1,2,3,4,5,6,7]
    "Activity":{
        "Type": "GlobalDictKeyListValueOperator+", #Activity type
        "KeyList": ['RobotRDPActive','ActivityList'], # RobotRDP Active  ActivityList
        "Value": gActivityROBOTStopList # ActivityList - see upper
    }
}
#Orchestrator settings update
def SettingsUpdate(inGSettings):
    #Add RobotRDPActive in control panel
    inGSettings["ControlPanelDict"]["RobotList"].append({"RenderFunction": ControlPanelRenderDict, "KeyStr": gControlPanelKeyStr}) 
    # Add scheduler activity in Scheduler.ActivityTimeList
    inGSettings["Scheduler"]["ActivityTimeList"].append(lActivityTimeItemRobotStart)
    inGSettings["Scheduler"]["ActivityTimeList"].append(lActivityTimeItemRobotStop)
    return inGSettings