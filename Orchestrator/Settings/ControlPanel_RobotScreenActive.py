import psutil
import datetime
import logging
import os
#DEFINITIONS
lProcessName = "OpenRPA_RobotScreenActive.exe"
lStartFilePath = os.path.join(os.getcwd(), "RobotScreenActive\\pyOpenRPA.Tools.RobotScreenActive_x64.cmd")
def RenderRobotScreenActive(inGlobalConfiguration):
    #Subheader Variants
    lSubheaderRunTrueText="State: <span style=\"color:green\">Turned on</span>"
    lSubheaderRunFalseText="State: <span style=\"color:red\">Turned off</span>"
    #Run button
    #Такое большое количество слэшей связано с тем, что этот текст отправляется сначала в браузер, рендерится там, а потом отправляется на процессор оркестратора
    import os
    lRobotScreenActivePath = lStartFilePath
    lOnClickRunButton=f"mGlobal.Controller.CMDRunText('start cmd /K  {lRobotScreenActivePath}');"
    # Activity update orchestrator
    lActivityOrchestratorBuildPath = os.getcwd()
    lActivityOrchestratorBuildPathEscaped = lActivityOrchestratorBuildPath.replace("\\","\\\\")
    lActivityOrchestratorUpdateStr = f"mGlobal.Controller.CMDRunText(\"taskkill /f /im OpenRPA_Orchestrator.exe & timeout 2 & cd {lActivityOrchestratorBuildPathEscaped} & git reset --hard & git pull origin master & pyOpenRPA.Orchestrator_x64_administrator_startup.cmd\");".replace("\"","&quot;")
    lActivityOrchestratorUpdateHTMLStr=f'<a onclick=\"{lActivityOrchestratorUpdateStr}\" style=\"color:orange\">Обновить orchestrator из GIT и перезапустить</a>'
    #Force close button
    lOnClickForceCloseButton=f"mGlobal.Controller.CMDRunText('taskkill /F /im {lProcessName}');"
    #Result template
    lResultDict={
        "HeaderLeftText":"Keep active screen session",
        "HeaderRightText":"Tech",
        "DataStorageKey":"RobotScreenActive", #Use key for set current dict in mGlobal.DataStorage["DtaaStorageKey"] on client side
        "SubheaderText":lSubheaderRunFalseText,
        "BodyKeyValueList":[
            {"Key":"Репозиторий","Value":lActivityOrchestratorUpdateHTMLStr}
        ],
        "FooterText":"Last update: 9:38:00 09.10.2019",
        "FooterButtonX2List":[
            {"Text":"Turn on", "Color":"green", "Link":"", "OnClick": lOnClickRunButton.replace("\\","\\\\")}
        ],
        "FooterButtonX1List":[
            {"Text":"Kill", "Color":"red", "Link":"", "OnClick": lOnClickForceCloseButton.replace("\\","\\\\")}
        ]
    }
    #Check if process running
    if CheckIfProcessRunning("OpenRPA_RobotScreenActive"):
        lResultDict["SubheaderText"]=lSubheaderRunTrueText
    #Process not running
    lResultDict["FooterText"]=f'Last update: {datetime.datetime.now().strftime("%H:%M:%S %d.%m.%Y")}'
    return lResultDict

def CheckIfProcessRunning(processName):
    '''
    Check if there is any running process that contains the given name processName.
    '''
    #Iterate over the all the running process
    for proc in psutil.process_iter():
        try:
            # Check if process name contains the given name string.
            if processName.lower() in proc.name().lower():
                return True
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
    return False;

#Orchestrator settings
def SettingsUpdate(inDict):
    #Add RobotRDPActive in control panel
    inDict["ControlPanelDict"]["RobotList"].append({"RenderFunction": RenderRobotScreenActive,"KeyStr":"RobotScreenActive"}) 
    return inDict