import datetime
# Init Section
gUserNameStr = "SUPERTOKEN" # User name
gDomainNameStr = "" # DOMAIN or EMPTY str
gDomainIsDefaultBool = False # If domain is exist and is default (default = you can type login without domain name)
gAuthTokenPermanentStr = "1992-04-03-0643-ru-b4ff-openrpa52zzz" # Set the permanent Auth token if it is needed, or empty
def SettingsUpdate(inDict):
    lRuleDomainUserDict = {
        "MethodMatchURLBeforeList": [
            {
                "Method":"GET",
                "MatchType":"Beginwith",
                "URL":"/",
                #"FlagAccessDefRequestGlobalAuthenticate": TestDef
                "FlagAccess": True
            },
            {
                "Method":"POST",
                "MatchType":"Beginwith",
                "URL":"/",
                #"FlagAccessDefRequestGlobalAuthenticate": TestDef
                "FlagAccess": True
            }
        ],
        "ControlPanelKeyAllowedList": ["TestControlPanel", "RobotRDPActive","RobotScreenActive", "ControlPanel_Template"] # If empty - all is allowed
    }
    # Case add domain + user
    inDict["Server"]["AccessUsers"]["RuleDomainUserDict"].update({(gDomainNameStr.upper(),gUserNameStr.upper()):lRuleDomainUserDict})
    if gDomainIsDefaultBool:
        # Case add default domain + user
        inDict["Server"]["AccessUsers"]["RuleDomainUserDict"].update({("",gUserNameStr.upper()):lRuleDomainUserDict})
    #"<AuthToken>":{"User":"", "Domain":"", "TokenDatetime":<Datetime>}
    #!!!!!!!!!!!!!!!!!!!!!!!
    #Attention: default supertoken is 1992-04-03-0643-ru-b4ff-openrpa52zzz - please change it when you will customize OpenRPA in your company
    #!!!!!!!!!!!!!!!!!!!!!!!
    inDict["Server"]["AccessUsers"]["AuthTokensDict"].update(
        {gAuthTokenPermanentStr:{"User":gUserNameStr, "Domain":gDomainNameStr, "TokenDatetime":  datetime.datetime.now(), "FlagDoNotExpire":True}}
    )
    #Return current dict
    return inDict