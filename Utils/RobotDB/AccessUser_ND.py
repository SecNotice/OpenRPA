def SettingsUpdate(inDict):
    ##################################################
    #""/"ND" MethodMatchURLList
    l__ND_RuleMethodMatchURLBeforeList={
        ("","ND"): {
            "MethodMatchURLBeforeList": [
                {
                    "Method":"GET",
                    "MatchType":"Beginwith",
                    "URL":"/",
                    #"FlagAccessDefRequestGlobalAuthenticate": TestDef
                    "FlagAccess": True
                },
                {
                    "Method":"POST",
                    "MatchType":"Beginwith",
                    "URL":"/",
                    #"FlagAccessDefRequestGlobalAuthenticate": TestDef
                    "FlagAccess": True
                }
            ]
        }
    }
    #Append to global list
    inDict["Server"]["AccessUsers"]["RuleDomainUserDict"].update(l__ND_RuleMethodMatchURLBeforeList)
    #Return current dict
    return inDict